#include <iostream>
#include <algorithm>

class Stack
{
    public:
        Stack(int size)
        {
            if(size > 0)
                values = new int[size];
            
            this->size = size;
        }

        Stack(const Stack& other) // <-- init 
        {

            values = new int[other.count];
            
            if(!other.empty())
                std::copy(other.values, &other.values[other.count], values);

            count = other.count;
            size = other.size;
        }

        inline bool empty() const
        {
            return (count <= 0) ? true : false;
        }

        inline bool full() const
        {
            return (count >= size) ? true : false;
        }

        inline void Pop()
        {
            if(!this->empty())
                count--;
        }

        inline int Size() const
        {
            return size;
        }

        inline int Count() const
        {
            return count;
        }

        void Push(int newValue)
        {
            if(!this->full())
                values[count++] = newValue;
            else    
            {
                int* resizeTmp = new int[2*size];
                std::copy(values, &values[count], resizeTmp);

                delete [] values;

                values = resizeTmp;
                values[count++] = newValue;
                resizeTmp = nullptr;
            }

        }

        inline int Top() const
        {
            if(!this->empty())
                return values[count-1];
            else
            {
                std :: cerr << "Stack :: Top() :: (error)stack is empty\n";
                return 1;
            }
        }   

        void destroy()
        {
            if(size > 0 && values != nullptr)
                delete[] values;
            
            values = nullptr;
        }  
        
        ~Stack()
        {
            if(size > 0 && values != nullptr)
              delete[] values;

            values = nullptr;
        }

    private:
        int* values = nullptr;
        int size = 0;
        int count = 0;
};


int main()
{
    //zadanie do stosu 3

    Stack stack1(10); 

    for(int i=0; i<stack1.Size(); i++)
        stack1.Push(i);
        
    Stack stack2(stack1);

    while(stack2.Count() > 0)
    {
        std :: cout << stack2.Top() << std :: endl;
        stack2.Pop();
    }

        
    // ZMIANY W KODZIE

    std :: cout <<  stack1.Top() << "ZMIANY W KODZIE\n";
   
    
    return 0;
}